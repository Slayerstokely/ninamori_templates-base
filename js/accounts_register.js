function username_checker() {
	if (document.getElementById('id_username').value && !window.checker_busy) {
		window.checker_busy = true;
		document.getElementById('id_username_checker').src = '/static/img/loading_36.gif';
		callAjax('{{ url_check_username }}?username='+document.getElementById('id_username').value, 'GET', username_checker_src)
	}
}

function username_checker_src(src) {
	document.getElementById('id_username_checker').next_src = src;
	setTimeout("document.getElementById('id_username_checker').src = document.getElementById('id_username_checker').next_src; window.checker_busy = false;", 1000);
}
