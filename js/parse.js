var Q = function () {
	var query_string = {};
	var query = window.location.search.substring(1);
	var vars = query.split("&");
	for (var i=0;i<vars.length;i++) {
		var pair = vars[i].split("=");
		if (typeof query_string[pair[0]] === "undefined") {
			query_string[pair[0]] = decodeURIComponent(pair[1]);
		} else if (typeof query_string[pair[0]] === "string") {
			var arr = [ query_string[pair[0]],decodeURIComponent(pair[1]) ];
			query_string[pair[0]] = arr;
		} else {
			query_string[pair[0]].push(decodeURIComponent(pair[1]));
		}
	}
	return query_string;
}();

(function(){
	function getRandomColor() {
		var letters = '0123456789ABCDEF'.split('');
		var color = '#';
		for (var i = 0; i < 6; i++ ) {
			color += letters[Math.floor(Math.random() * 16)];
		}
		return color;
	}

	function getRandomBrightColor() {
		r = Math.floor(153+Math.random()*102);
		g = Math.floor(153+Math.random()*102);
		b = Math.floor(153+Math.random()*102);
		color = '#' + r.toString(16) + g.toString(16) + b.toString(16);
		return color;
	}

	polls = document.getElementsByClassName('poll');
	for (i=0; i<polls.length; i++){
		votebars = polls[i].getElementsByClassName('votebar');
		topvotes = 0;
		for (j=0; j<votebars.length; j++){
			if (topvotes < parseInt(votebars[j].dataset.votesCounter)) {
				topvotes = parseInt(votebars[j].dataset.votesCounter);
			}
			votebars[j].style.backgroundColor = getRandomBrightColor();
		}
		for (j=0; j<votebars.length; j++){
			percentage = (votebars[j].dataset.votesCounter*100 / topvotes);
			calc_width = 'calc(' + percentage + '% - 10px * ' + percentage/100 + ')';
			votebars[j].style.width = calc_width;
		}
	}

	if (document.getElementById('feedback_form')) {
		restore_feedback_from_cookie();
		setInterval('save_feedback_to_cookie()', 1000);
	}

	if (docCookies.getItem('mod_buttons')) {
		document.getElementById('toggle_comments_moderation').classList.add('button-success');
		toggle_comments_moderation(true);
	}

	if (('ontouchstart' in window) || (navigator.maxTouchPoints > 0) || (navigator.msMaxTouchPoints > 0)) {
		sheet = document.styleSheets[0];
		sheet.insertRule('.comments .comment .controls-box .controls {display: block !important; font-size: 125%;}', sheet.cssRules.length);
	}

})();
