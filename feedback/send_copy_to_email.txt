{% spaceless %}
{% load i18n %}
{% blocktrans with site_name=site_name channel=channel sender_email=sender_email message=message %}This is a copy of your feedback message for [{{ site_name }}] about [{{ channel }}]

{{ message }}

Your email address should be: {{ sender_email }}{% endblocktrans %}
{% endspaceless %}
