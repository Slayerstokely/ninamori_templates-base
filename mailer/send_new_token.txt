{% spaceless %}
{% load i18n %}
{% blocktrans with protocol=protocol site_domain=site_domain app_name=app_name app_manage=app_manage token=token email=email %}You have requested access token change for [{{ app_name }}]

Here is a link for your subscription management with new access token:
{{ protocol }}://{{ site_domain }}{{ app_manage }}?token={{ token }}
This link has no expiration date and should be valid unless you change your token.
Do not share this link.

Your email address should be: {{ email }}{% endblocktrans %}
{% endspaceless %}
